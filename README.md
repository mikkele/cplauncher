#CpLauncher

##Purpose
CpLauncher is a tool that helps you manage multiple CrashPlan remote servers from a single Windows GUI.

The CrashPlan GUI will by default manage the local CrashPlan backup service running on the same PC as the GUI. 
It is however possible to setup the CrashPlan backup service on a remote computer and configure that
from your local Windows PC, as described in the following links: 

* [Using CrashPlan On A Headless Computer](http://support.code42.com/CrashPlan/Latest/Configuring/Using_CrashPlan_On_A_Headless_Computer)
* [CrashPlan packages for Synology NAS](http://pcloadletter.co.uk/2012/01/30/crashplan-syno-package/)

Managing this manually is however quite difficult, expecially since the addition of the .ui_info file requirement in the 
most recent CrashPlan releases. The CpLauncher tool aims to make this process a bit more user-friendly.


##Features

* Keeps a library of defined CrashPlan server definition files.
* Allow you to select which CrashPlan server you want to connect to.
* Automatically copy the required CrashPlan server definition files to the correct locations before launching the CrashPlan GUI for you.

The configuration files handled are these:

* The __.ui_info__ file located in the _C:\ProgramData\CrashPlan_ directory. 
This file contains the port and server GUID for the remote CrashPlan server.
* The __ui_properties__ file located in the _C:\Program Files(x86)\CrashPlan\conf_ directory. 
 This file contain the IP address of the remote CrashPlan server.

##Usage
TODO

##Dependencies and Requirements
CpLauncher is written in C# and uses WPF. It thus require the Microsoft .NET Framework version 4.0 or later.

Since copying the CrashPlan configuration files require access to protected Windows directories 
the CpLauncher will request administrative rights when it starts. If you do not have administrative rights on your PC
you probably have no business managing backup jobs on remote servers either.

##License
The source code for CpLaucher is released under the [MIT License](http://opensource.org/licenses/MIT)

    Copyright (c) 2015 Mikkel Elmholdt

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.

