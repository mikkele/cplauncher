﻿using System.IO;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CpLauncher.Util
{
    /// <summary>
    /// A generic class that handles saving and reading classes 
    /// to and from a JSON representation.
    /// </summary>
    public abstract class JsonSerializerBase<T>
    {
        /// <summary>
        /// Load a class instance from a JSON text file.
        /// </summary>
        public virtual T LoadFromFile(string fullPathName)
        {
            using (var sr = new StreamReader(fullPathName))
            {
                var jsonData = sr.ReadToEnd();

                var serializerSettings = new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Objects
                };

                serializerSettings.Converters.Add(new StringEnumConverter());


                return JsonConvert.DeserializeObject<T>(jsonData, serializerSettings); 
            }
        }

        /// <summary>
        /// Save a class instance to a JSON text file.
        /// </summary>
        public virtual bool SaveToFile(T testConfig, string fullPathName)
        {
            var fileDir = Path.GetDirectoryName(fullPathName);
            if (string.IsNullOrEmpty(fileDir))
                return false;

            if (!Directory.Exists(fileDir))
            {
                Directory.CreateDirectory(fileDir);
            }

            var serializerSettings = new JsonSerializerSettings();
            serializerSettings.Converters.Add(new StringEnumConverter());

            var jsonData = JsonConvert.SerializeObject(testConfig, Formatting.Indented,
                                                       serializerSettings);

            using (var sw = new StreamWriter(fullPathName))
            {
                sw.Write(jsonData);
            }

            return true;
        }
    }
}