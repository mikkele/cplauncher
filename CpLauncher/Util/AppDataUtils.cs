﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace CpLauncher.Util
{
    public static class AppDataUtils
    {
        private const string UnknownName = "<unknown>";

        private static object GetEntryAssemblyAttributes(Type attributeType)
        {
            Assembly entry = Assembly.GetEntryAssembly();
            if (entry == null)
                return null;

            object[] attributes = entry.GetCustomAttributes(attributeType, false);

            if (attributes.Length == 0)
            {
                throw new ArgumentException(@"No attributes found", attributeType.ToString());
            }
            return attributes[0];
        }

        public static Version AppVersion
        {
            get { return Assembly.GetEntryAssembly().GetName().Version; }
        }

        /// <summary>
        /// The short product version
        /// </summary>
        public static string AppProductVersion
        {
            get { return AppVersion.ToString(); }
        }

        public static string AppFileVersion
        {
            get
            {
                var fvi = FileVersionInfo.GetVersionInfo(Assembly.GetEntryAssembly().Location);
                return fvi.FileVersion;
            }
        }

        /// <summary>
        /// The application informational version
        /// </summary>
        public static string AppInfoVersion
        {
            get
            {
                var assembly = Assembly.GetEntryAssembly();
                var attrib = Attribute.GetCustomAttribute(assembly, typeof(AssemblyInformationalVersionAttribute))
                    as AssemblyInformationalVersionAttribute;

                return attrib != null ? attrib.InformationalVersion : AppProductVersion;
            }
        }

        public static string AppProductName
        {
            get
            {
                var attrib = GetEntryAssemblyAttributes(typeof(AssemblyProductAttribute));
                var assemblyProductAttribute = attrib as AssemblyProductAttribute;
                if (assemblyProductAttribute != null)
                    return assemblyProductAttribute.Product;
                return "";
            }
        }

        public static string AppProductDescription
        {
            get
            {
                var attrib = GetEntryAssemblyAttributes(typeof(AssemblyDescriptionAttribute)) as AssemblyDescriptionAttribute;
                return attrib != null ? attrib.Description : UnknownName;
            }
        }

        public static string AppCompanyName
        {
            get
            {
                object attrib = GetEntryAssemblyAttributes(typeof(AssemblyCompanyAttribute));
                if (attrib == null)
                    return "";
                var companyAttribute = attrib as AssemblyCompanyAttribute;
                return companyAttribute != null ? companyAttribute.Company : UnknownName;
            }
        }

        public static string AppCopyright
        {
            get
            {
                var copyrightAttribute = GetEntryAssemblyAttributes(typeof(AssemblyCopyrightAttribute)) as AssemblyCopyrightAttribute;
                return copyrightAttribute != null ? copyrightAttribute.Copyright : UnknownName;
            }
        }

        public static string GetAppDataFolder()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        }

        public static string GetCompanyDataDir()
        {
            return Path.Combine(GetAppDataFolder(), AppCompanyName);
        }

        public static string GetAppDataDir()
        {
            return Path.Combine(GetCompanyDataDir(), AppProductName);
        }

        
    }
}