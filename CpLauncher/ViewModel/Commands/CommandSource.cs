﻿using System.Windows.Media;

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace CpLauncher.ViewModel.Commands
{
    /// <summary>
    /// A special type for viewmodel properties which represents a
    /// command source control. This will usually be rendered as
    /// a button by the view. 
    /// 
    /// When the source control is triggered (for instance when the 
    /// rendered button is pressed) the associated command will be
    /// executed.
    /// </summary>
    public class CommandSource : ViewModelBase
    {
        private string m_Text;
        private ImageSource m_Image;
        private RelayCommand m_Command;
        private int m_Width;

        /// <summary>
        /// Text to display for the command control
        /// </summary>
        public string Text
        {
            get { return m_Text; }
            set
            {
                m_Text = value;
                RaisePropertyChanged(() => Text);
            }
        }

        public int Width
        {
            get { return m_Width; }
            set
            {
                m_Width = value;
                RaisePropertyChanged(() => Width);
            }
        }

        /// <summary>
        /// An optional image
        /// </summary>
        public ImageSource Image
        {
            get { return m_Image; }
            set
            {
                m_Image = value;
                RaisePropertyChanged(() => Image);
            }
        }

        /// <summary>
        /// The command to execute
        /// </summary>
        public RelayCommand Command
        {
            get { return m_Command; }
            set
            {
                m_Command = value;
                RaisePropertyChanged(() => Command);
            }
        }
    }
}
