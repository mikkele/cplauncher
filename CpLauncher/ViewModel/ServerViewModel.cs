﻿using CpLauncher.Model;

using GalaSoft.MvvmLight;

namespace CpLauncher.ViewModel
{
    public class ServerViewModel : ViewModelBase
    {
        private readonly CpServerInfo m_BaseModel;

        public string ServerId => m_BaseModel.ServerId;

        public string IpAddress => m_BaseModel.IpAddress;

        public int PortNo => m_BaseModel.PortNo;


        public ServerViewModel(CpServerInfo baseModel)
        {
            m_BaseModel = baseModel;
        }

    }
}