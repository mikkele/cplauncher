using System;
using System.Collections.ObjectModel;

using CpLauncher.Model;
using CpLauncher.Util;

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace CpLauncher.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private readonly CpServerCollection m_ServerCollection;
        private ServerViewModel m_SelectedServer;

        #region Viewmodel properties

        public string ApplicationName 
        {
            // TODO - use appdatautils to get this from Assembly
            get 
            {
                return string.Format("{0} - v{1}", 
                    AppDataUtils.AppProductDescription, AppDataUtils.AppInfoVersion); 
            }
        }

        public ServerViewModel SelectedServer
        {
            get { return m_SelectedServer; }
            set
            {
                m_SelectedServer = value;
                RaisePropertyChanged(() => SelectedServer);

                LaunchServerCommand.RaiseCanExecuteChanged();
            }
        }

        public ObservableCollection<ServerViewModel> DefinedServers { get; private set; }

        #endregion

        #region Commands

        public RelayCommand HelpRequestedCommand { get; private set; }

        public RelayCommand LaunchServerCommand { get; private set; }

        public RelayCommand RefreshServerListCommand { get; private set; }

        public RelayCommand RemoveServerCommand { get; private set; }

        #endregion

        #region Events

        /// <summary>
        /// Event to request an application shutdown from this viewmodel
        /// </summary>
        public event EventHandler<EventArgs> RequestClose;

        protected virtual void OnRequestClose()
        {
            var handler = RequestClose;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            m_ServerCollection = new CpServerCollection();
            DefinedServers = new ObservableCollection<ServerViewModel>();

            HelpRequestedCommand = new RelayCommand(HelpRequestedExecuted);
            LaunchServerCommand = new RelayCommand(LaunchServerExecuted, CanExecuteLaunchServer);
            RefreshServerListCommand = new RelayCommand(RefreshServerListExecuted);
            RemoveServerCommand = new RelayCommand(RemoveServerExecuted, CanExecuteRemoveServer);

            RefreshServerListExecuted();
        }

        private void HelpRequestedExecuted()
        {
            // TODO
        }

        public void OnApplicationCloseExecuted()
        {
            CloseApplication();
        }

        public void CloseApplication()
        {
            OnRequestClose();
        }

        private bool CanExecuteLaunchServer()
        {
            return SelectedServer != null && !m_ServerCollection.IsCpRunning();
        }

        private void LaunchServerExecuted()
        {
            if (m_ServerCollection.LaunchServer(SelectedServer.ServerId))
            {
                OnRequestClose();
            }
        }

        private void RefreshServerListExecuted()
        {
            m_ServerCollection.ReadDefinedServers();

            DefinedServers.Clear();
            foreach (var server in m_ServerCollection.CpServers)
            {
                DefinedServers.Add(new ServerViewModel(server));
            }
        }

        private bool CanExecuteRemoveServer()
        {
            return SelectedServer != null;
        }

        private void RemoveServerExecuted()
        {
            // TODO
        }

    }
}