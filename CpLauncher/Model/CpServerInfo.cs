﻿using System.IO;
using System.Linq;

namespace CpLauncher.Model
{
    public class CpServerInfo
    {
        public string ServerId { get; private set; }

        public string ServerDefPath { get; private set; }

        public string IpAddress { get; private set; }

        public int PortNo { get; private set; }


        public CpServerInfo(string defPath)
        {
            ServerId = defPath.Split(Path.DirectorySeparatorChar).Last();

            ServerDefPath = defPath;

            // TODO - parse IP address and port no

        }
    }
}