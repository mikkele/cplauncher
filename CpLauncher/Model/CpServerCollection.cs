﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

using CpLauncher.Util;

namespace CpLauncher.Model
{
    public class CpServerCollection
    {
        #region Constants

        public const string CrashPlanDirName = "CrashPlan";
        public const string CrashPlanConfDirName = "conf";
        public const string CrashPlanExeName = "CrashPlanDesktop.exe";

        public const string ServerDataDefDir = "Servers";
        public const string BackupDataDefDir = "Backup";
        public const string UiPropertiesFileName = "ui.properties";
        public const string UiInfoFileName = ".ui_info";

        #endregion

        private readonly string m_AppDataDir;
        private readonly string m_ServerDataDir;

        private readonly string m_CpUiAppDir;
        private readonly string m_CpUiExePath;
        private readonly string m_CpAppDataDir;

        /// <summary>
        /// List of defined servers
        /// </summary>
        public List<CpServerInfo> CpServers { get; set; }

        /// <summary>
        /// Ctor
        /// </summary>
        public CpServerCollection()
        {
            m_CpAppDataDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData),
                CrashPlanDirName);

            m_CpUiAppDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), 
                CrashPlanDirName);

            m_CpUiExePath = Path.Combine(m_CpUiAppDir, CrashPlanExeName);

            m_AppDataDir = AppDataUtils.GetAppDataDir();
            Directory.CreateDirectory(m_AppDataDir);

            m_ServerDataDir = Path.Combine(m_AppDataDir, ServerDataDefDir);
            Directory.CreateDirectory(m_ServerDataDir);

            CpServers = new List<CpServerInfo>();
        }

        public bool IsCpInstalled()
        {
            if (!Directory.Exists(m_CpUiAppDir))
                return false;

            return File.Exists(m_CpUiExePath);
        }

        /// <summary>
        /// Return true if the CrashPlan UI executable is currently running
        /// </summary>
        /// <returns></returns>
        public bool IsCpRunning()
        {
            var procList = Process.GetProcessesByName(Path.GetFileNameWithoutExtension(CrashPlanExeName));
            return (procList.Length > 0);
        }

        private string GetUiPropertiesFilePath(string dirpath)
        {
            return Path.Combine(dirpath, UiPropertiesFileName);
        }

        private string GetUiInfoFilePath(string dirpath)
        {
            return Path.Combine(dirpath, UiInfoFileName);
        }

        private bool IsValidServerDir(string dirpath)
        {
            if (!File.Exists(GetUiPropertiesFilePath(dirpath)))
                return false;
            if (!File.Exists(GetUiInfoFilePath(dirpath)))
                return false;

            return true;
        }

        public void ReadDefinedServers()
        {
            CpServers.Clear();

            var serverDirs = Directory.EnumerateDirectories(m_ServerDataDir);
            foreach (var dirpath in serverDirs)
            {
                var fullPath = Path.GetFullPath(dirpath).TrimEnd(Path.DirectorySeparatorChar);

                if (!IsValidServerDir(fullPath))
                    continue;

                CpServers.Add(new CpServerInfo(fullPath));
            }
        }

        private Process GetCpProcess()
        {
            return new Process
            {
                StartInfo =
                {
                    FileName = m_CpUiExePath,
                    WorkingDirectory = m_CpUiAppDir
                }
            };
        }

        /// <summary>
        /// Make a backup of the specified file if the backlup does not already exist
        /// </summary>
        private void BackupFile(string filename, string origDir, string backupDir)
        {
            var backupFilePath = Path.Combine(backupDir, filename);
            if (File.Exists(backupFilePath))
                return;

            var origFilePath = Path.Combine(origDir, filename);
            if (File.Exists(origFilePath))
            {
                File.Copy(origFilePath, backupFilePath, false);
            }
        }

        /// <summary>
        /// Make a backup of the original CP definition files
        /// </summary>
        private void BackupOriginalDefinitions()
        {
            var backupDir = Path.Combine(m_AppDataDir, BackupDataDefDir);
            Directory.CreateDirectory(backupDir);

            BackupFile(UiPropertiesFileName, Path.Combine(m_CpUiAppDir, CrashPlanConfDirName), backupDir);
            BackupFile(UiInfoFileName, m_CpAppDataDir, backupDir);
        }

        private void CopyFile(string fileName, string serverDir, string destDir)
        {
            var srcFilePath = Path.Combine(serverDir, fileName);
            var destFilePath = Path.Combine(destDir, fileName);

            File.Copy(srcFilePath, destFilePath, true);
        }

        private void CopyServerDefinitions(CpServerInfo serverInfo)
        {
            BackupOriginalDefinitions();

            CopyFile(UiPropertiesFileName, serverInfo.ServerDefPath, Path.Combine(m_CpUiAppDir, CrashPlanConfDirName));
            CopyFile(UiInfoFileName, serverInfo.ServerDefPath, m_CpAppDataDir);
        }

        public bool LaunchServer(string serverId)
        {
            if (!IsCpInstalled())
                return false;

            var serverDef = CpServers.Find(item => item.ServerId == serverId);
            if (serverDef == null)
                return false;

            CopyServerDefinitions(serverDef);

            return GetCpProcess().Start();
        }

    }
}