﻿using System;
using System.ComponentModel;
using System.Windows;

using CpLauncher.View;
using CpLauncher.ViewModel;

namespace CpLauncher
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private MainViewModel m_MainViewModel;
        private MainWindow m_MainWindow;

        public static App MainApp { get { return (App)Current; } }

        /// <summary>
        /// Static reference to the main application view
        /// </summary>
        public static MainWindow GetMainWindow()
        {
            return MainApp.m_MainWindow;
        }


        protected override void OnStartup(StartupEventArgs e)
        {
            m_MainViewModel = ViewModelLocator.GetInstance().MainViewModel;
            m_MainViewModel.RequestClose += ViewModelRequestClose;

            // Create main application window
            m_MainWindow = new MainWindow();
            m_MainWindow.Loaded += OnMainWindowLoaded;
            m_MainWindow.Closing += OnMainWindowClosing;

            // Show and focus main window
            m_MainWindow.Show();
            m_MainWindow.Activate();

            base.OnStartup(e);
   
        }

        private void OnMainWindowLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            //m_MainViewModel.MainViewLoaded();
        }

        private void ViewModelRequestClose(object sender, EventArgs e)
        {
            m_MainWindow.Close();
        }

        private void OnMainWindowClosing(object sender, CancelEventArgs e)
        {
            //m_MainViewModel.CleanupForClose();

            //m_MainViewModel.SaveCurrConfig();

            //SaveAppPosAndSize();
            //SettingsHandler.Save();

            ViewModelLocator.Cleanup();

            m_MainWindow.Closing -= OnMainWindowClosing;
            m_MainViewModel.RequestClose -= ViewModelRequestClose;
        }

    }
}
